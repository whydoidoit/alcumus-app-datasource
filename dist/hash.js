"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.hash = hash;
exports.compareHash = compareHash;

var _xxhashjs = _interopRequireDefault(require("xxhashjs"));

var _jsonStableStringify = _interopRequireDefault(require("json-stable-stringify"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SEED = 0xABC1234D;
var charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-+_!@£$%^&*()={}[]|\\;: <>?.,`~";

function convertToString(number) {
  if (number < charset.length) {
    return charset[number];
  } else {
    return convertToString(Math.floor(number / charset.length)) + charset[number % charset.length];
  }
}

function skipId(key, value) {
  return key === "_id" ? undefined : value;
}

function hash() {
  var any = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var textVersion = (0, _jsonStableStringify.default)(any, {
    replacer: skipId
  }) || "";
  return "".concat(convertToString(textVersion.length), ":").concat(convertToString(_xxhashjs.default.h64(textVersion, SEED)));
}

function compareHash() {
  var any = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var hash = arguments.length > 1 ? arguments[1] : undefined;
  var textVersion = (0, _jsonStableStringify.default)(any, {
    replacer: skipId
  }) || "";

  if (!("" + hash).startsWith(convertToString(textVersion.length))) {
    return false;
  }

  return "".concat(convertToString(textVersion.length), ":").concat(convertToString(_xxhashjs.default.h64(textVersion, SEED))) === hash;
}