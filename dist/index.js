"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "mutate", {
  enumerable: true,
  get: function get() {
    return _mutations.mutate;
  }
});
Object.defineProperty(exports, "clear", {
  enumerable: true,
  get: function get() {
    return _mutations.clear;
  }
});
Object.defineProperty(exports, "mutations", {
  enumerable: true,
  get: function get() {
    return _mutations.mutations;
  }
});
Object.defineProperty(exports, "assign", {
  enumerable: true,
  get: function get() {
    return _mutations.assign;
  }
});
Object.defineProperty(exports, "set", {
  enumerable: true,
  get: function get() {
    return _mutate.set;
  }
});
Object.defineProperty(exports, "merge", {
  enumerable: true,
  get: function get() {
    return _mutate.merge;
  }
});
Object.defineProperty(exports, "events", {
  enumerable: true,
  get: function get() {
    return _mutate.events;
  }
});
Object.defineProperty(exports, "remove", {
  enumerable: true,
  get: function get() {
    return _mutate.remove;
  }
});
Object.defineProperty(exports, "create", {
  enumerable: true,
  get: function get() {
    return _mutate.create;
  }
});
Object.defineProperty(exports, "applyMutation", {
  enumerable: true,
  get: function get() {
    return _mutate.mutate;
  }
});

var _mutations = require("./mutations");

var _mutate = require("./mutate");