"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.withoutHashes = withoutHashes;
exports.mutate = mutate;
exports.set = set;
exports.Settable = Settable;
exports.create = create;
exports.merge = merge;
exports.default = exports.lastMutationId = exports.events = void 0;

var _get = _interopRequireDefault(require("lodash/get"));

var _forEach = _interopRequireDefault(require("lodash/forEach"));

var _set2 = _interopRequireDefault(require("lodash/set"));

var _isObject = _interopRequireDefault(require("lodash/isObject"));

var _merge2 = _interopRequireDefault(require("lodash/merge"));

var _isString = _interopRequireDefault(require("lodash/isString"));

var _eventemitter = require("eventemitter2");

var _shortid = require("shortid");

var _hash = require("./hash");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var events = new _eventemitter.EventEmitter2({
  wildcard: true,
  maxListeners: 0
});
exports.events = events;
var noHashes = false;

function conflict(item, mutation) {
  var message = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "Hash does not match";
  var params = {
    item: item,
    mutation: mutation,
    mode: 'throw'
  };
  events.emit("conflict.".concat(mutation.type), params);

  switch (params.mode) {
    case 'suppress':
      return false;

    case 'continue':
      return true;

    default:
      throw new Error(message);
  }
}

function hash(any) {
  if (noHashes) return undefined;
  return (0, _hash.hash)(any);
}

function withoutHashes(fn) {
  try {
    noHashes = true;
    fn();
  } finally {
    noHashes = false;
  }
}

function hashPath(path) {
  var array = path.indexOf(']');
  var dot = path.indexOf('.');
  array = array === -1 ? path.length : array;
  dot = dot === -1 ? path.length : dot;
  return path.slice(0, Math.min(dot, array) + 1);
}

function _set(item, mutation) {
  var $ = mutation.$,
      path = mutation.path,
      value = mutation.value,
      _ = mutation._;

  if ($) {
    if (!(0, _hash.compareHash)((0, _get.default)(item, hashPath(path)), $)) {
      if (!conflict(item, mutation, "Hash does not match for path '".concat(path, "'"))) return;
    }
  }

  if (_) {
    var _id = mutation._id;
    (0, _forEach.default)(_, function (mutation, path) {
      try {
        _set(item, _objectSpread({}, mutation, {
          path: path,
          _id: _id
        }));
      } catch (e) {
        mutation.error = e;
        throw e;
      }
    });
  } else {
    (0, _set2.default)(item, path, value);
    return hash(value);
  }
}

function _merge(item, mutation) {
  var value = mutation.value,
      $ = mutation.$;
  if ($ && !(0, _hash.compareHash)(item, $) && !conflict(item, mutation)) return;
  (0, _merge2.default)(item, value);
}

function _create(item, mutation) {
  var _mutation$state = mutation.state,
      state = _mutation$state === void 0 ? {} : _mutation$state,
      _mutation$itemType = mutation.itemType,
      itemType = _mutation$itemType === void 0 ? "" : _mutation$itemType;
  Object.keys(item).forEach(function (key) {
    delete item[key];
  });
  state._id = state._id || (0, _shortid.generate)();

  if (!state._id.endsWith(":" + itemType)) {
    state._id = state._id + ":" + itemType;
  }

  Object.assign(item, state);
  mutation._id = state._id;
  return item;
}

var MutationActions = {
  _set: _set,
  _merge: _merge,
  _create: _create
};
var lastMutationId = "";
exports.lastMutationId = lastMutationId;

function mutate(item) {
  var mutation = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var record = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
  if (!(0, _isObject.default)(item)) throw new Error("".concat(mutation.type || '', " failed item should be an object"));

  try {
    mutation._id = mutation._id || item._id;
    record && events.emit("before.mutate", {
      item: item,
      mutation: mutation
    });
    MutationActions["_".concat(mutation.type)](item, mutation);
    record && events.emit("after.mutate", {
      item: item,
      mutation: mutation
    });
  } catch (e) {
    throw new Error("".concat(mutation.type, " failed with \"").concat(e.message, "\" inner stack:\n").concat(e.stack));
  }
}

function set(path, value, existingValue) {
  var result = {
    type: 'set',
    path: path,
    value: value,
    uq: (0, _shortid.generate)()
  };

  if (existingValue !== undefined) {
    if ((0, _isObject.default)(existingValue)) {
      if (existingValue.$) {
        var _value = (0, _get.default)(existingValue.$, hashPath(path));

        result.$ = hash(_value);
      } else {
        var _value2 = (0, _get.default)(existingValue, hashPath(path));

        result.$ = hash(_value2);
        _value2 !== result.value && mutate(existingValue, result);
      }
    } else {
      result.$ = hash(existingValue);
    }
  }

  return result;
}

function Settable(initial) {
  Object.assign(this, initial);
}

Settable.prototype.set = function (path, value) {
  if ((0, _isObject.default)(path)) {
    var _arr = Object.entries(path);

    for (var _i = 0; _i < _arr.length; _i++) {
      var _arr$_i = _slicedToArray(_arr[_i], 2),
          key = _arr$_i[0],
          _value3 = _arr$_i[1];

      set(key, _value3, this);
    }
  } else {
    set(path, value, this);
  }

  return this;
};

function create(type, state) {
  var item = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  if (!type) throw new Error("You must specify a type when creating an item");
  if (!(0, _isString.default)(type)) throw new Error("The type should be a string");
  var result = {
    type: 'create',
    itemType: type,
    state: state,
    uq: (0, _shortid.generate)()
  };
  mutate(item, result);
  create._result = result;
  return item;
}

function merge(value, item) {
  var result = {
    type: 'merge',
    value: value,
    uq: (0, _shortid.generate)()
  };

  if (item) {
    result.$ = hash(item.$ || item);
    !item.$ && mutate(item, result);
  }

  return result;
}

var SetAdaptor = {
  frame: function frame(frames) {
    var currentFrame = frames[frames.length - 1];
    if (currentFrame && currentFrame.type === 'set') return currentFrame;
    var frame = {
      type: 'set',
      _: {},
      uq: []
    };
    frames.push(frame);
    return frame;
  },
  record: function record(frame, mutation) {
    mutation = JSON.parse(JSON.stringify(mutation));
    var existing = frame._[mutation.path];
    frame.uq.push(mutation.uq);
    delete mutation.uq;

    if (existing) {
      existing.value = mutation.value;
    } else {
      var copied = _objectSpread({}, mutation);

      delete copied.path;
      delete copied._id;
      frame._[mutation.path] = copied;
    }
  }
};
events.on("config.set", function (params) {
  params.adaptor = SetAdaptor;
});
var _default = mutate;
exports.default = _default;