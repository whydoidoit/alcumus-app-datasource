"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.clear = clear;
exports.assign = assign;
exports.mutate = mutate;
exports.default = exports.mutations = void 0;

var _isObject = _interopRequireDefault(require("lodash/isObject"));

var _shortid = require("shortid");

var _mutate = require("./mutate");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var mutations = {};
exports.mutations = mutations;

function clear() {
  exports.mutations = mutations = {};
}

var DefaultAdaptor = {
  frame: function frame(frames) {
    var frame = {};
    frames.push(frame);
    return frame;
  },
  record: function record(frame, mutation) {
    Object.assign(frame, mutation);
  }
};
var running = false;

_mutate.events.on("after.mutate", function (_ref) {
  var item = _ref.item,
      mutation = _ref.mutation;
  !running && mutate(item, mutation, true);
});

function assign(target, source) {
  Object.entries(source).forEach(function (_ref2) {
    var _ref3 = _slicedToArray(_ref2, 2),
        key = _ref3[0],
        value = _ref3[1];

    (0, _mutate.set)(key, value, target);
  });
  return target;
}

function mutate(item, mutation) {
  var skip = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

  if (!(0, _isObject.default)(item) || Array.isArray(item)) {
    throw new Error("item should be an object");
  }

  if (!(0, _isObject.default)(mutation) || !mutation.type) {
    if (Array.isArray(mutation)) {
      var errors = [];
      running = skip;
      mutation.forEach(function (mutation) {
        try {
          skip ? (0, _mutate.mutate)(item, mutation, false) : mutate(item, mutation, skip);
        } catch (e) {
          mutation.error = e;
          errors.push(e);
        }
      });
      running = false;

      if (errors.length) {
        throw new Error("Not all mutations were applied:\n" + errors.join("\n"));
      }

      return;
    }

    throw new Error("Must supply a mutation");
  }

  try {
    running = true;
    !skip && (0, _mutate.mutate)(item, mutation, false);
    var id = item._id = item._id || (0, _shortid.generate)();
    var settings = {
      adaptor: DefaultAdaptor
    };

    _mutate.events.emit("config.".concat(mutation.type), settings);

    var frames = mutations[id] = mutations[id] || [];
    var frame = settings.adaptor.frame(frames);
    settings.adaptor.record(frame, mutation);

    _mutate.events.emit("mutated", {
      item: item,
      mutations: mutations,
      mutation: mutation
    });
  } finally {
    running = false;
  }
}

var _default = mutate;
exports.default = _default;