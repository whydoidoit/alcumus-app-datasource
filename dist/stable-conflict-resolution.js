"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.enable = enable;
exports.disable = disable;

var _mutate = require("./mutate");

var _hash = require("./hash");

var _get = _interopRequireDefault(require("lodash/get"));

var _isEqual = _interopRequireDefault(require("lodash/isEqual"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var enabled = false;

function resolver(conflict) {
  var item = conflict.item,
      mutation = conflict.mutation;

  switch (mutation.type) {
    case 'set':
      var value = (0, _get.default)(item, mutation.path);

      if ((0, _isEqual.default)(value, mutation.value)) {
        conflict.mode = 'suppress';
        break;
      }

      var hashedValue = (0, _hash.hash)(value);
      var setHash = (0, _hash.hash)(mutation.value);
      conflict.mode = setHash.localeCompare(hashedValue) > 0 ? 'continue' : 'suppress';
      break;

    default:
      //Should throw
      break;
  }
}

function enable() {
  if (enabled) return;
  enabled = true;

  _mutate.events.on('conflict.*', resolver);
}

function disable() {
  if (!enabled) return;
  enabled = false;

  _mutate.events.removeAllListeners('conflict.*');
}

enable();