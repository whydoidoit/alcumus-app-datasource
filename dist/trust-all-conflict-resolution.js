"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.enable = enable;
exports.disable = disable;

var _mutate = require("./mutate");

var enabled = false;

function resolver(conflict) {
  conflict.mode = 'continue';
}

function enable() {
  if (enabled) return;
  enabled = true;

  _mutate.events.on('conflict.*', resolver);
}

function disable() {
  if (!enabled) return;
  enabled = false;

  _mutate.events.removeAllListeners('conflict.*');
}

enable();