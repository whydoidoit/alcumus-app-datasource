module.exports = function (wallaby) {
    console.log("WALLABY", __dirname)
    return {
        files: [
            'src/**/*.+(js|jsx|json|snap|css|less|sass|scss|jpg|jpeg|gif|png|svg)',
            '!src/**/*.test.js'
        ],

        tests: ['src/**/*.test.js?(x)', 'src/*.test.js?(x)'],

        env: {
            type: 'node',
            runner: 'node'
        },

        compilers: {
            '**/*.js?(x)': wallaby.compilers.babel({
                presets: ['@babel/preset-env']
            })
        },

        setup: wallaby => {
            // const jestConfig = require('./jest.config');
            // Object.keys(jestConfig.transform || {}).forEach(k => ~k.indexOf('^.+\\.(js|jsx') && void delete jestConfig.transform[k]);
            // delete jestConfig.testEnvironment;
            // wallaby.testFramework.configure(jestConfig);
        },

        testFramework: 'mocha'
    };
};
