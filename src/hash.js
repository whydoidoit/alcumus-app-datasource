import XXH from "xxhashjs"
import stringify from "json-stable-stringify"

const SEED = 0xABC1234D

const charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-+_!@£$%^&*()={}[]|\\;: <>?.,`~"

function convertToString(number) {
    if(number < charset.length) {
        return charset[number]
    } else {
        return convertToString(Math.floor(number/charset.length)) + charset[number % charset.length]
    }
}

function skipId(key, value) {
    return key === "_id" ? undefined : value
}

export function hash(any = null) {
    let textVersion = stringify(any, {replacer: skipId}) || ""
    return `${convertToString(textVersion.length)}:${convertToString(XXH.h64(textVersion, SEED))}`
}

export function compareHash(any = null, hash) {
    let textVersion = stringify(any, {replacer: skipId}) || ""
    if (!("" + hash).startsWith(convertToString(textVersion.length))) {
        return false
    }
    return `${convertToString(textVersion.length)}:${convertToString(XXH.h64(textVersion, SEED))}` === hash
}
