import {hash, compareHash} from "./hash"
import {expect} from "chai"

describe("Hashing", function() {
    it("should create a hash of an object", function() {
        expect(hash({a: 1, b: 3}).length).to.be.gt(2)
    })
    it("should create a hash of a primitive", function () {
        expect(hash(1.133).length).to.be.gt(2)
    })
    it("should make matching hashes", function() {
        expect(hash(1.1333)).to.be.eq(hash(1.1333))
    })
    it("should be able to compare hashes and match", function() {
        expect(compareHash({a: 1, b:3}, hash({a: 1, b:3}))).to.eq(true)
    })
    it("should be able to compare hashes and fail a match", function () {
        expect(compareHash({a: 1, b: 3}, hash({a: 1, b: 3342423423}))).to.eq(false)
    })
    it("should ignore _id keys in when hashing", function() {
        expect(compareHash({a: 1, _id: "2"}, hash({a: 1, _id: "1"}))).to.eq(true)
    })
})
