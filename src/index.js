import {mutate, clear, mutations, assign} from './mutations'
import {set, merge, events, remove, create, mutate as applyMutation} from "./mutate"

export {mutate, set, merge, clear, mutations, assign, events, remove, create, applyMutation}
