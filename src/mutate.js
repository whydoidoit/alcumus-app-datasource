import getValue from "lodash/get"
import forEach from "lodash/forEach"
import setValue from "lodash/set"
import isObject from "lodash/isObject"
import mergeValue from "lodash/merge"
import isString from "lodash/isString"
import {EventEmitter2} from "eventemitter2"
import {generate} from "shortid"
import {compareHash, hash as calculateHash} from "./hash"

export const events = new EventEmitter2({wildcard: true, maxListeners: 0})

let noHashes = false

function conflict(item, mutation, message = "Hash does not match") {
    let params = {item, mutation, mode: 'throw'}
    events.emit(`conflict.${mutation.type}`, params)
    switch (params.mode) {
        case 'suppress':
            return false
        case 'continue':
            return true
        default:
            throw new Error(message)
    }
}

function hash(any) {
    if (noHashes) return undefined
    return calculateHash(any)
}

export function withoutHashes(fn) {
    try {
        noHashes = true
        fn()
    } finally {
        noHashes = false
    }
}

function hashPath(path) {
    let array = path.indexOf(']')
    let dot = path.indexOf('.')
    array = array === -1 ? path.length : array
    dot = dot === -1 ? path.length : dot
    return path.slice(0, Math.min(dot, array) + 1)
}

function _set(item, mutation) {
    const {$, path, value, _} = mutation
    if ($) {
        if (!compareHash(getValue(item, hashPath(path)), $)) {
            if (!conflict(item, mutation, `Hash does not match for path '${path}'`)) return
        }
    }
    if (_) {
        let {_id} = mutation
        forEach(_, (mutation, path) => {
            try {
                _set(item, {...mutation, path, _id})
            } catch (e) {
                mutation.error = e
                throw e
            }
        })
    } else {
        setValue(item, path, value)
        return hash(value)
    }
}

function _merge(item, mutation) {
    const {value, $} = mutation
    if ($ && !compareHash(item, $) && !conflict(item, mutation)) return
    mergeValue(item, value)
}

function _create(item, mutation) {
    const {state = {}, itemType = ""} = mutation
    Object.keys(item).forEach(key => {
        delete item[key]
    })
    state._id = state._id || generate()
    if(!state._id.endsWith(":" + itemType)) {
        state._id = state._id + ":" + itemType
    }
    Object.assign(item, state)
    mutation._id = state._id
    return item
}

const MutationActions = {
    _set,
    _merge,
    _create
}

export let lastMutationId = ""

export function mutate(item, mutation = {}, record = true) {
    if (!isObject(item)) throw new Error(`${mutation.type || ''} failed item should be an object`)
    try {
        mutation._id = mutation._id || item._id
        record && events.emit(`before.mutate`, {item, mutation})
        MutationActions[`_${mutation.type}`](item, mutation)
        record && events.emit(`after.mutate`, {item, mutation})
    } catch (e) {
        throw new Error(`${mutation.type} failed with "${e.message}" inner stack:\n${e.stack}`)
    }
}

export function set(path, value, existingValue) {
    let result = {
        type: 'set',
        path,
        value,
        uq: generate()
    }
    if (existingValue !== undefined) {
        if (isObject(existingValue)) {
            if (existingValue.$) {
                let value = getValue(existingValue.$, hashPath(path))
                result.$ = hash(value)
            } else {
                let value = getValue(existingValue, hashPath(path))

                result.$ = hash(value)
                value !== result.value && mutate(existingValue, result)
            }
        } else {
            result.$ = hash(existingValue)
        }
    }
    return result
}

export function Settable(initial) {
    Object.assign(this, initial)
}

Settable.prototype.set = function(path, value) {
    if(isObject(path)) {
        for(let [key, value] of Object.entries(path)) {
            set(key, value, this)
        }
    } else {
        set(path, value, this)
    }
    return this
}


export function create(type, state, item = {}) {
    if (!type) throw new Error("You must specify a type when creating an item")
    if(!isString(type)) throw new Error("The type should be a string")
    let result = {
        type: 'create',
        itemType: type,
        state,
        uq: generate()
    }
    mutate(item, result)
    create._result = result
    return item
}

export function merge(value, item) {
    let result = {
        type: 'merge',
        value,
        uq: generate()
    }
    if (item) {
        result.$ = hash(item.$ || item)
        !item.$ && mutate(item, result)
    }
    return result
}

const SetAdaptor = {
    frame: function (frames) {
        let currentFrame = frames[frames.length - 1]
        if (currentFrame && currentFrame.type === 'set') return currentFrame
        let frame = {type: 'set', _: {}, uq: []}
        frames.push(frame)
        return frame
    },
    record: function (frame, mutation) {
        mutation = JSON.parse(JSON.stringify(mutation))
        let existing = frame._[mutation.path]
        frame.uq.push(mutation.uq)
        delete mutation.uq
        if (existing) {
            existing.value = mutation.value
        } else {
            let copied = {...mutation}
            delete copied.path
            delete copied._id
            frame._[mutation.path] = copied
        }
    }
}

events.on("config.set", function (params) {
    params.adaptor = SetAdaptor
})


export default mutate
