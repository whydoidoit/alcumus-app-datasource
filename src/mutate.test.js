import {expect} from "chai"
import {create, insert, merge, move, mutate, pop, push, remove, set, events} from "./mutate"
import {hash} from "./hash"
import {disable} from './stable-conflict-resolution'

describe("Mutate", function () {
    beforeEach(disable)
    beforeEach(()=>{
        events.removeAllListeners("conflict.*")
    })
    it("should fail to mutate something that is not an object", function () {
        expect(() => mutate("hello", {type: 'anything'})).to.throw("item should be an object")
    })
    it("should fail with an unknown mutation", function () {
        expect(() => mutate({}, {type: "banana"})).to.throw("banana failed")
    })
    it("should mutate an object without a hash", function () {
        const item = {a: 1}
        const mutation = {type: 'set', path: 'a', value: 2}
        mutate(item, mutation)
        expect(item.a).to.eq(2)
    })
    it("should mutate an object with a hash", function () {
        const item = {a: 1}
        const mutation = {type: 'set', path: 'a', value: 2, $: hash(item.a)}
        mutate(item, mutation)
        expect(item.a).to.eq(2)
    })
    it("should fail to mutate an object with a hash that misses", function () {
        const item = {a: 1}
        const mutation = {type: 'set', path: 'a', value: 2, $: "1"}
        expect(() => mutate(item, mutation)).to.throw("set failed")
        expect(item.a).to.eq(1)
    })
    it("should be able to set a value that doesn't exist", function () {
        const item = {a: 1}
        const mutation = {type: 'set', path: 'b', value: 2}
        mutate(item, mutation)
        expect(item.b).to.eq(2)
    })
    it("should let us use a function to create the mutation", function () {
        const item = {a: 1}
        mutate(item, set("b", 2))
        expect(item.b).to.eq(2)
    })
    it("should let us use a function and an existing value to create a mutation", function () {
        const item = {a: 1}
        set("a", 2, item)
        expect(item.a).to.eq(2)
    })
    it("should be able to set an item in an array", function () {
        const item = {a: 1, b: [{c: 1}, {c: 2}]}
        set("b[0].c", 2, item)
        expect(item.b[0].c).to.eq(2)
    })
    it("should be able to set a missing array item", function () {
        const item = {a: 1, b: [{c: 1}, {c: 2}]}
        set("b[3].c", 2, item)
        expect(item.b[3].c).to.eq(2)
        expect(item.b.length).to.eq(4)
    })
    it("should set and check hashes on the first object in the path", function () {
        const item = {a: 1, b: [{c: 1}, {c: 2}]}
        const mutation = set("b[0].c", 2, item)
        item.b[0].c = 9
        expect(() => mutate(item, mutation)).to.throw("Hash does not match")
        expect(item.b[0].c).to.eq(9)
    })
    it("should set and check hashes on the first object in the path using specific value hash", function () {
        const item = {a: 1, b: [{c: 1}, {c: 2}]}
        const mutation = set("b[0].c", 2, 2)
        item.b[0].c = 9
        expect(() => mutate(item, mutation)).to.throw("Hash does not match")
        expect(item.b[0].c).to.eq(9)
    })
    it("should be able to merge", function () {
        const item = {b: [{a: 1}, {a: 2}]}
        mutate(item, merge({a: 1, b: [undefined, undefined, {a: 3}]}))
        expect(item.a).to.eq(1)
        expect(item.b[2].a).to.eq(3)
        expect(item.b[1].a).to.eq(2)
        expect(item.b[0].a).to.eq(1)
    })
    it("should be able to merge with a matching hash", function () {
        const item = {b: [{a: 1}, {a: 2}]}
        mutate(item, merge({a: 1, b: [undefined, undefined, {a: 3}]}, {$: item}))
        expect(item.a).to.eq(1)
        expect(item.b[2].a).to.eq(3)
        expect(item.b[1].a).to.eq(2)
        expect(item.b[0].a).to.eq(1)
    })
    it("should be able to merge with a matching hash using inline merge", function () {
        const item = {b: [{a: 1}, {a: 2}]}
        merge({a: 1, b: [undefined, undefined, {a: 3}]}, item)
        expect(item.a).to.eq(1)
        expect(item.b[2].a).to.eq(3)
        expect(item.b[1].a).to.eq(2)
        expect(item.b[0].a).to.eq(1)
    })
    it("should not be able to merge with a mismatching hash", function () {
        const item = {b: [{a: 1}, {a: 2}]}
        let mutation = merge({a: 1, b: [undefined, undefined, {a: 3}]}, item)
        item.a = 3
        expect(() => mutate(item, mutation)).to.throw("Hash does not match")
        expect(item.a).to.eq(3)
    })
    it("should be able to merge with a mismatching hash if overridden by a conflict resolver", function () {
        const item = {b: [{a: 1}, {a: 2}]}
        let mutation = merge({a: 1, b: [undefined, undefined, {a: 3}]}, item)
        item.a = 3
        events.on("conflict.*", function (params) {
            params.mode = 'suppress'
        })
        expect(() => mutate(item, mutation)).not.to.throw("Hash does not match")
    })
    it("should not mutate a primitive", function () {
        expect(() => mutate(1)).to.throw("item should be an object")
    })
    it("should not be able to create an item without a type", function() {
        expect(()=>create()).to.throw("must specify a type")
    })
    it("should require a type to be a string on creation", function() {
        expect(()=>create(1)).to.throw("type should be a string")
    })
    it("should create a new item", function () {
        let item = create("test", {test: 1})
        expect(item._id.length).to.be.gt(1)
        expect(item._id).to.include(":test")
        expect(item.test).to.eq(1)
    })
    it("should wipe the contents of an existing item when creating", function() {
        let item = create("test", {test: 1}, {banana: 1})
        expect(item.banana).to.eq(undefined)
    })

})
