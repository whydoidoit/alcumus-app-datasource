import isObject from "lodash/isObject"
import {generate} from "shortid"
import {events, mutate as execute, set} from "./mutate"

export let mutations = {}

export function clear() {
    mutations = {}
}

const DefaultAdaptor = {
    frame(frames) {
        let frame = {}
        frames.push(frame)
        return frame
    },
    record(frame, mutation) {
        Object.assign(frame, mutation)
    }
}

let running = false

events.on("after.mutate", function ({item, mutation}) {
    !running && mutate(item, mutation, true)
})

export function assign(target, source) {
    Object.entries(source).forEach(function ([key, value]) {
        set(key, value, target)
    })
    return target
}


export function mutate(item, mutation, skip = false) {
    if (!isObject(item) || Array.isArray(item)) {
        throw new Error("item should be an object")
    }
    if (!isObject(mutation) || !mutation.type) {
        if (Array.isArray(mutation)) {
            const errors = []
            running = skip
            mutation.forEach(mutation => {
                try {
                    skip ? execute(item, mutation, false) : mutate(item, mutation, skip)
                } catch (e) {
                    mutation.error = e
                    errors.push(e)
                }
            })
            running = false
            if (errors.length) {
                throw new Error("Not all mutations were applied:\n" + errors.join("\n"))
            }
            return
        }
        throw new Error("Must supply a mutation")
    }
    try {
        running = true
        !skip && execute(item, mutation, false)
        let id = item._id = item._id || generate()
        let settings = {adaptor: DefaultAdaptor}
        events.emit(`config.${mutation.type}`, settings)
        let frames = mutations[id] = mutations[id] || []
        let frame = settings.adaptor.frame(frames)
        settings.adaptor.record(frame, mutation)
        events.emit(`mutated`, {item, mutations, mutation})
    } finally {
        running = false
    }

}

export default mutate
