import mutate, {assign, clear, mutations} from "./mutations"
import {assert, expect} from "chai"
import {create, events, push, set, withoutHashes} from "./mutate"
import {hash} from "./hash"
import sinon from "sinon"
import {disable} from './stable-conflict-resolution'

describe("Mutations", function () {
    beforeEach(() => {
        clear()
        disable()
    })
    it("should not be able to record a mutation unless supplied an object and a mutation", function () {
        expect(() => mutate()).to.throw("should be an object")
        expect(() => mutate(1)).to.throw("should be an object")
        expect(() => mutate([{a: 1}])).to.throw("should be an object")
        expect(() => mutate({a: 1})).to.throw("Must supply a mutation")
        expect(() => mutate({a: 1}, 1)).to.throw("Must supply a mutation")
        expect(() => mutate({a: 1}, {})).to.throw("Must supply a mutation")
        expect(() => mutate({a: 1}, {type: 'set'})).not.to.throw()
    })
    it("should add an _id to the object if not present", function () {
        const item = {}
        mutate(item, set("a", 1))
        expect(item._id.length).to.be.gt(3)
        let id = item._id
        mutate(item, set("b", 2))
        expect(item._id).to.eq(id)
    })
    it("should record a set mutation based on the object", function () {
        const item = {_id: 'a'}
        mutate(item, set("b", 1))
        expect(mutations.a.length).to.eq(1)
        expect(mutations.a[0]._.b.path).to.eq("b")
        expect(mutations.a[0]._.b.value).to.eq(1)
        mutate(item, set("b", 2))
        expect(mutations.a.length).to.eq(1)
        expect(mutations.a[0]._.b.value).to.eq(2)
        mutate(item, set("c", 1))
        expect(mutations.a.length).to.eq(1)
        expect(mutations.a[0]._.c.value).to.eq(1)
    })
    it("should record a mutation if using an object for the existing item", function () {
        const item = {_id: 'test', c: [], b: 0}
        set("b", 2, item)
        expect(mutations.test.length).to.eq(1)
    })
    it("should not record a mutation if using a hash object for the existing item", function () {
        const item = {_id: 'test', c: [], b: 0}
        set("b", 2, {$: item})
        expect(mutations.test).to.eq(undefined)
    })
    it("should handle multiple frames when a frame needs insertion", function () {
        const item = {_id: 'test', c: []}
        mutate(item, set("b", 1))
        expect(mutations.test.length).to.eq(1)
        mutate(item, push("c", {a: 1}))
        expect(mutations.test.length).to.eq(2)
        mutate(item, push("c", {a: 1}))
        expect(mutations.test.length).to.eq(3)
        mutate(item, set("b", 2))
        expect(mutations.test.length).to.eq(4)
        mutate(item, set("b", 3))
        expect(mutations.test.length).to.eq(4)
    })
    it("should not update the hash if not updating the frame", function () {
        const item = {_id: 'test', c: [], b: 0}
        let $ = hash(item.b)
        mutate(item, set("b", 1, {$: item}))
        expect(mutations.test.length).to.eq(1)
        expect(mutations.test[0]._.b.$).to.eq($)
        set("b", 2, item)
        expect(mutations.test[0]._.b.$).to.eq($)
        mutate(item, push("c", {a: 1}))
        mutate(item, set("b", 2, {$: item}))
        expect(mutations.test.length).to.eq(3)
        expect(mutations.test[mutations.test.length - 1]._.b.$).not.to.eq($)
    })
    it("should not record hashes when hashing is turned off", function () {
        const item = {_id: 'test', c: [], b: 0}
        withoutHashes(() => {
            set("b", 2, item)
            expect(mutations.test[0]._.b.$).to.eq(undefined)
        })
    })
    it("should apply a multi-set mutation", function () {
        const item1 = {_id: 'test', b: 0}
        const item2 = {b: 0}
        set("a", 1, item1)
        set("b", 2, item1)
        mutate(item2, mutations.test[0])
        expect(item2.a).to.eq(1)
        expect(item2.b).to.eq(2)
    })
    it("should apply an array of mutations", function () {
        const item1 = {_id: 'test', b: 0, c: []}
        const item2 = {_id: 'test2', b: 0, c: []}
        set("a", 1, item1)
        set("b", 2, item1)
        push("c", {a: 1}, item1)
        set("d", 3, item1)
        mutate(item2, mutations.test)
        expect(item2.a).to.eq(1)
        expect(item2.b).to.eq(2)
        expect(item2.d).to.eq(3)
        expect(item2.c[0].a).to.eq(1)
    })
    it("should apply an array of mutations and not record the changes to the target is skip is true", function () {
        const item1 = {_id: 'test', b: 0, c: []}
        const item2 = {_id: 'test2', b: 0, c: []}
        set("a", 1, item1)
        set("b", 2, item1)
        push("c", {a: 1}, item1)
        set("d", 3, item1)
        mutate(item2, mutations.test, true)
        expect(mutations.test2).to.eq(undefined)
        expect(item2.a).to.eq(1)
        expect(item2.b).to.eq(2)
        expect(item2.d).to.eq(3)
        expect(item2.c[0].a).to.eq(1)
    })
    it("should not apply an element in an array of mutations if a hash is not matching for that element", function () {
        const item1 = {_id: 'test', b: 0, c: []}
        const item2 = {_id: 'test2', b: 0, c: [], d: 1}
        set("a", 1, item1)
        set("b", 2, item1)
        set("d", 3, item1)
        push("c", {a: 1}, item1)
        expect(() => mutate(item2, mutations.test)).to.throw("Hash does not match")
        expect(mutations.test[0].error.message).to.include("Hash does not match")
        expect(mutations.test[0].error.message).to.include("'d'")
        expect(mutations.test[0]._.d.error.message).to.include("Hash does not match")
        expect(mutations.test[0]._.d.error.message).to.include("'d'")
        expect(item2.a).to.eq(1)
        expect(item2.b).to.eq(2)
        expect(item2.d).to.eq(1)
        expect(item2.c[0].a).to.eq(1)
    })
    it("should allow the application of multiple properties with assign", function () {
        const item1 = {_id: 'test', b: 0, c: [], d: 1}
        const item2 = {_id: 'test2', b: 0, c: [], d: 1}
        assign(item1, {
            a: 3,
            b: 2,
            c: [{a: 1}],
            d: 2
        })
        mutate(item2, mutations.test, true)
        expect(item2.c[0].a).to.eq(1)
        expect(item2.a).to.eq(3)
        expect(item2.b).to.eq(2)
        expect(item2.d).to.eq(2)
    })
    it("raises a mutated event when changes are made", function () {
        let callback = sinon.spy()
        events.on("mutated", callback)
        const item1 = {_id: 'test', b: 0, c: [], d: 1}
        const item2 = {_id: 'test2', b: 0, c: [], d: 1}
        set("a", 2, item1)
        assert(callback.calledOnce)
        mutate(item2, mutations.test, true)
        assert(callback.calledOnce)
    })
    it("raises an event to indicate conflict", function () {
        let callback = sinon.spy()
        events.on("conflict.*", callback)
        const item = {a: 1}
        const mutation = {type: 'set', path: 'a', value: 2, $: "1"}
        expect(() => mutate(item, mutation)).to.throw("set failed")
        assert(callback.calledOnce)
    })
    it("should allow conflict event to suppress the inbound value", function () {
        events.on("conflict.*", function (params) {
            params.mode = 'suppress'
        })
        const item = {a: 1}
        const mutation = {type: 'set', path: 'a', value: 2, $: "1"}
        expect(() => mutate(item, mutation)).not.to.throw("set failed")
        expect(item.a).to.eq(1)
    })
    it("should allow conflict event to pass the inbound value", function () {
        events.on("conflict.*", function (params) {
            params.mode = 'continue'
        })
        const item = {a: 1}
        const mutation = {type: 'set', path: 'a', value: 2, $: "1"}
        expect(() => mutate(item, mutation)).not.to.throw("set failed")
        expect(item.a).to.eq(2)
    })
    it("should record the creation of a new item", function () {
        let item = create("test",{test: 1})
        expect(mutations[item._id][0].type).to.eq('create')
    })

})
