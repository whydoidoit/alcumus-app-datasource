import {events} from "./mutate"
import {hash} from "./hash"
import get from "lodash/get"
import isEqual from "lodash/isEqual"

let enabled = false

function resolver(conflict) {
    const {item, mutation} = conflict
    switch(mutation.type) {
        case 'set':
            let value = get(item, mutation.path)
            if(isEqual(value, mutation.value)) {
                conflict.mode = 'suppress'
                break
            }
            let hashedValue = hash(value)
            let setHash = hash(mutation.value)
            conflict.mode = setHash.localeCompare(hashedValue) > 0 ? 'continue' : 'suppress'
            break
        default:
            //Should throw
            break
    }
}

export function enable() {
    if(enabled) return
    enabled = true
    events.on('conflict.*', resolver)
}

export function disable() {
    if(!enabled) return
    enabled = false
    events.removeAllListeners('conflict.*')
}

enable()

