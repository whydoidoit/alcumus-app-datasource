import {remove} from "./mutate"
import {expect} from "chai"
import mutate from "./mutations"
import {enable,disable} from './stable-conflict-resolution'
import {clear} from "./mutations"

describe("Stable conflict resolution", function() {
    beforeEach(()=>{
        clear()
        enable()
    })
    afterEach(disable)
    it("should suppress errors in set functions stable type 1", function() {
        const item = {a: 1}
        const mutation = {type: 'set', path: 'a', value: 2, $: "1"}
        expect(() => mutate(item, mutation)).not.to.throw()
        expect(item.a).to.eq(2)
    })
    it("should suppress errors in set functions stable type 2", function () {
        const item = {a: 1}
        const mutation = {type: 'set', path: 'a', value: 990, $: "1"}
        expect(() => mutate(item, mutation)).not.to.throw()
        expect(item.a).to.eq(990)
    })
    it("should not have an error if the values are equal", function() {
        const item = {a: 1}
        const mutation = {type: 'set', path: 'a', value: 2, $: "1"}
        item.a = 2
        expect(() => mutate(item, mutation)).not.to.throw()
        expect(item.a).to.eq(2)
    })
    it("should throw an error if doing a non-set mutation", function() {
        const item = {b: [{a: 1}]}
        const mutation = remove("b", 0, {$: item})
        item.b.push({a: 2})
        expect(() => mutate(item, mutation)).to.throw("Hash does not match")
        expect(item.b.length).to.eq(2)
    })
    it("should not suppress when disabled", function() {
        disable()
        const item = {a: 1}
        const mutation = {type: 'set', path: 'a', value: 2, $: "1"}
        expect(() => mutate(item, mutation)).to.throw()
        expect(item.a).to.eq(1)
    })
})
