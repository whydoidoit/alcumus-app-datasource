import {events} from "./mutate"

let enabled = false

function resolver(conflict) {
    conflict.mode = 'continue'
}

export function enable() {
    if (enabled) return
    enabled = true
    events.on('conflict.*', resolver)
}

export function disable() {
    if (!enabled) return
    enabled = false
    events.removeAllListeners('conflict.*')
}

enable()

